package com.franktran.employeeservice.service;

import com.franktran.domain.entity.Employee;
import com.franktran.employeeservice.dao.EmployeeDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

  private final EmployeeDao employeeDao;

  public EmployeeService(EmployeeDao employeeDao) {
    this.employeeDao = employeeDao;
  }

  public List<Employee> getEmployees() {
    return employeeDao.findAll();
  }

  public Employee getEmployeeById(int id) {
    return employeeDao.findById(id);
  }
}
