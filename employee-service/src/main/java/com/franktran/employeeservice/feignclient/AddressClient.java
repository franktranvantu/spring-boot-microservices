package com.franktran.employeeservice.feignclient;

import com.franktran.domain.dto.AddressDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "address-service", path = "/address-service/api")
public interface AddressClient {

  @GetMapping("/address/{id}")
  AddressDto getAddressByEmployeeId(@PathVariable int id);
}
