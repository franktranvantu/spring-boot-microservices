package com.franktran.employeeservice.controller;

import com.franktran.domain.dto.AddressDto;
import com.franktran.domain.dto.EmployeeDto;
import com.franktran.domain.entity.Employee;
import com.franktran.employeeservice.feignclient.AddressClient;
import com.franktran.employeeservice.service.EmployeeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class EmployeeController {

//  private final RestTemplate restTemplate;
//  private final WebClient webClient;
  @Autowired
  private AddressClient addressClient;
  private final EmployeeService employeeService;
  private final ModelMapper modelMapper;

//  public EmployeeController(@Value("${addressservice.base.url}") String addressBaseUrl,
//                            RestTemplateBuilder builder,
//                            EmployeeService employeeService,
//                            ModelMapper modelMapper) {
//    this.restTemplate = builder.rootUri(addressBaseUrl).build();
//    this.employeeService = employeeService;
//    this.modelMapper = modelMapper;
//  }

//  public EmployeeController(WebClient webClient, EmployeeService employeeService, ModelMapper modelMapper) {
//    this.webClient = webClient;
//    this.employeeService = employeeService;
//    this.modelMapper = modelMapper;
//  }

  public EmployeeController(
                            EmployeeService employeeService,
                            ModelMapper modelMapper) {
    this.employeeService = employeeService;
    this.modelMapper = modelMapper;
  }

  @GetMapping("/employees")
  public ResponseEntity<List<EmployeeDto>> getEmployees() {
    List<Employee> employees = employeeService.getEmployees();
    List<EmployeeDto> employeeDtos = employees
        .stream()
        .map(employee -> {
          EmployeeDto employeeDto = modelMapper.map(employee, EmployeeDto.class);
          AddressDto addressDto = addressClient.getAddressByEmployeeId(employee.getId());
          employeeDto.setAddress(addressDto);
          return employeeDto;
        })
        .collect(Collectors.toList());
    return ResponseEntity.ok(employeeDtos);
  }

  @GetMapping("/employees/{id}")
  public ResponseEntity<EmployeeDto> getEmployeeById(@PathVariable int id) {
    String url = "/address/{id}";
    Employee employee = employeeService.getEmployeeById(id);
//    AddressDto addressDto = restTemplate.getForObject(url, AddressDto.class, id);
//    AddressDto addressDto = webClient
//        .get()
//        .uri(url, id).retrieve()
//        .bodyToMono(AddressDto.class)
//        .block();
    AddressDto addressDto = addressClient.getAddressByEmployeeId(id);
    EmployeeDto employeeDto = modelMapper.map(employee, EmployeeDto.class);
    employeeDto.setAddress(addressDto);
    return ResponseEntity.ok(employeeDto);
  }
}
