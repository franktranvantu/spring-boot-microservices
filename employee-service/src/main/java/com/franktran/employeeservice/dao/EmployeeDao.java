package com.franktran.employeeservice.dao;

import com.franktran.domain.entity.Employee;
import com.franktran.domain.entity.EmployeeRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmployeeDao {

  private final JdbcTemplate jdbcTemplate;
  private final EmployeeRowMapper employeeRowMapper;

  public EmployeeDao(JdbcTemplate jdbcTemplate, EmployeeRowMapper employeeRowMapper) {
    this.jdbcTemplate = jdbcTemplate;
    this.employeeRowMapper = employeeRowMapper;
  }

  public List<Employee> findAll() {
    String sql = "SELECT * FROM employee";
    return jdbcTemplate.query(sql, employeeRowMapper);
  }

  public Employee findById(int id) {
    String sql = "SELECT * FROM employee WHERE id = ?";
    return jdbcTemplate.queryForObject(sql, employeeRowMapper, id);
  }
}
