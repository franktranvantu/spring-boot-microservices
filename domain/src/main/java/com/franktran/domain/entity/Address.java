package com.franktran.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class Address {
  private int id;
  @NonNull
  private String line1;
  @NonNull
  private String line2;
  @NonNull
  private String city;
  @NonNull
  private String country;
}
