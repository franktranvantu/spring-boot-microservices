package com.franktran.domain.entity;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AddressRowMapper implements RowMapper<Address> {
  /**
   * Implementations must implement this method to map each row of data
   * in the ResultSet. This method should not call {@code next()} on
   * the ResultSet; it is only supposed to map values of the current row.
   *
   * @param rs     the ResultSet to map (pre-initialized for the current row)
   * @param rowNum the number of the current row
   * @return the result object for the current row (may be {@code null})
   * @throws SQLException if an SQLException is encountered getting
   *                      column values (that is, there's no need to catch SQLException)
   */
  @Override
  public Address mapRow(ResultSet rs, int rowNum) throws SQLException {
    int id = rs.getInt("id");
    String line1 = rs.getString("line1");
    String line2 = rs.getString("line2");
    String city = rs.getString("city");
    String country = rs.getString("country");
    return new Address(id, line1, line2, city, country);
  }
}
