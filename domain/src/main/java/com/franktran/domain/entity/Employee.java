package com.franktran.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class Employee {
  private int id;
  @NonNull
  private String name;
  @NonNull
  private String email;
  @NonNull
  private String bloodGroup;
  private int addressId;
}
