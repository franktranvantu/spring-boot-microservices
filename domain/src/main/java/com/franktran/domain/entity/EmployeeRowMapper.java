package com.franktran.domain.entity;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeRowMapper implements RowMapper<Employee> {
  /**
   * Implementations must implement this method to map each row of data
   * in the ResultSet. This method should not call {@code next()} on
   * the ResultSet; it is only supposed to map values of the current row.
   *
   * @param rs     the ResultSet to map (pre-initialized for the current row)
   * @param rowNum the number of the current row
   * @return the result object for the current row (may be {@code null})
   * @throws SQLException if an SQLException is encountered getting
   *                      column values (that is, there's no need to catch SQLException)
   */
  @Override
  public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
    int id = rs.getInt("id");
    String name = rs.getString("name");
    String email = rs.getString("email");
    String bloodGroup = rs.getString("blood_group");
    int addressId = rs.getInt("address_id");
    return new Employee(id, name, email, bloodGroup, addressId);
  }
}
