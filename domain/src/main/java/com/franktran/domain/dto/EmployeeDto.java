package com.franktran.domain.dto;

import lombok.Data;

@Data
public class EmployeeDto {
  private int id;
  private String name;
  private String email;
  private String bloodGroup;
  private AddressDto address;
}
