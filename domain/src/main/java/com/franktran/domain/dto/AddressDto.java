package com.franktran.domain.dto;

import lombok.Data;

@Data
public class AddressDto {
  private int id;
  private String line1;
  private String line2;
  private String city;
  private String country;
}
