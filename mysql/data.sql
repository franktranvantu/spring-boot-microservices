INSERT INTO address (line1, line2, city, country) VALUES ('456 Phan Huy Ich', 'Go Vap', 'Ho Chi Minh', 'Vietnam');
INSERT INTO address (line1, line2, city, country) VALUES ('123 Duong Thi Muoi', '12', 'Ho Chi Minh', 'Vietnam');
INSERT INTO address (line1, line2, city, country) VALUES ('9A Thanh Xuan', '12', 'Ho Chi Minh', 'Vietnam');

INSERT INTO employee(name, email, blood_group, address_id) VALUES ('Tu', 'tu@gmail.com', 'A', 3);
INSERT INTO employee(name, email, blood_group, address_id) VALUES ('Tai', 'tai@gmail.com', 'B', 2);
INSERT INTO employee(name, email, blood_group, address_id) VALUES ('Hieu', 'hieu@gmail.com', 'A+', 1);
