package com.franktran.addressservice.dao;

import com.franktran.domain.entity.Address;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class AddressDaoTest extends BaseDaoTest {

  private AddressDao underTest;

  private static final List<Address> ADDRESSES = List.of(
      new Address(1, "456 Phan Huy Ich", "Go Vap", "Ho Chi Minh", "Vietnam"),
      new Address(2, "123 Duong Thi Muoi", "12", "Ho Chi Minh", "Vietnam"),
      new Address(3, "9A Thanh Xuan", "12", "Ho Chi Minh", "Vietnam")
  );

  @BeforeEach
  public void beforeEach() {
    underTest = new AddressDao(jdbcTemplate);
  }

  @Test
  public void whenFindAll_thenReturnCorrectAddresses() {

    List<Address> addresses = underTest.findAll();

    assertThat(addresses)
        .hasSize(3)
        .containsAll(ADDRESSES);
  }

  @Test
  public void givenExistedId_whenFindById_thenReturnCorrectAddress() {
    Address actualAddress = underTest.findById(2).get();
    Address expectedAddress = ADDRESSES.get(1);

    assertThat(actualAddress)
        .isNotNull()
        .usingRecursiveComparison()
        .isEqualTo(expectedAddress);
  }

  @Test
  public void givenExistedEmployeeId_whenFindByEmployeeId_thenReturnCorrectAddress() {
    Address actualAddress = underTest.findByEmployeeId(1).get();
    Address expectedAddress = ADDRESSES.get(2);

    assertThat(actualAddress)
        .isNotNull()
        .usingRecursiveComparison()
        .isEqualTo(expectedAddress);
  }

  @Test
  public void givenNonExistedId_whenFindById_thenReturnEmpty() {
    Optional<Address> actualAddress = underTest.findById(5);

    assertThat(actualAddress).isEqualTo(Optional.empty());
  }

  @Test
  public void givenNonExistedEmployeeId_whenFindByEmployeeId_thenReturnEmpty() {
    Optional<Address> actualAddress = underTest.findByEmployeeId(5);

    assertThat(actualAddress).isEqualTo(Optional.empty());
  }
}
