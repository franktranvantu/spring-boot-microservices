package com.franktran.addressservice.dao;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

public class BaseDaoTest {

  protected static JdbcTemplate jdbcTemplate;

  @BeforeAll
  public static void beforeAll() {
    DataSource dataSource = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2)
        .addScripts("classpath:jdbc/schema.sql", "classpath:jdbc/data.sql")
        .build();

    jdbcTemplate = new JdbcTemplate(dataSource);
  }
}
