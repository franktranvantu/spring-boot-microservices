CREATE TABLE address(
    id INT AUTO_INCREMENT PRIMARY KEY,
    line1 VARCHAR (50),
    line2 VARCHAR (50),
    city VARCHAR (50),
    country VARCHAR (50)
);

CREATE TABLE employee(
     id INT AUTO_INCREMENT PRIMARY KEY,
     name VARCHAR (50),
     email VARCHAR (50),
     blood_group VARCHAR (50),
     address_id INT
);

ALTER TABLE employee ADD FOREIGN KEY (address_id) REFERENCES address(id);
