package com.franktran.addressservice.service;

import com.franktran.addressservice.dao.AddressDao;
import com.franktran.domain.entity.Address;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {

  private final AddressDao addressDao;

  public AddressService(AddressDao addressDao) {
    this.addressDao = addressDao;
  }

  public Address getAddressById(int id) {
    return addressDao.findById(id).get();
  }

  public List<Address> getAddresses() {
    return addressDao.findAll();
  }

  public Address getAddressByEmployeeId(int employeeId) {
    return addressDao.findByEmployeeId(employeeId).get();
  }
}
