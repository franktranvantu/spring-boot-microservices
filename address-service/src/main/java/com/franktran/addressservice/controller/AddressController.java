package com.franktran.addressservice.controller;

import com.franktran.addressservice.service.AddressService;
import com.franktran.domain.dto.AddressDto;
import com.franktran.domain.entity.Address;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class AddressController {

  private final AddressService addressService;
  private final ModelMapper modelMapper;

  public AddressController(AddressService addressService, ModelMapper modelMapper) {
    this.addressService = addressService;
    this.modelMapper = modelMapper;
  }

  @GetMapping("/addresses")
  public ResponseEntity<List<AddressDto>> getAddresses() {
    List<Address> addresses = addressService.getAddresses();
    List<AddressDto> addressDtos = addresses
        .stream()
        .map(address -> modelMapper.map(address, AddressDto.class))
        .collect(Collectors.toList());
    return ResponseEntity.ok(addressDtos);
  }

  @GetMapping("/addresses/{id}")
  public ResponseEntity<AddressDto> getAddressById(@PathVariable int id) {
    Address address = addressService.getAddressById(id);
    AddressDto addressDto = modelMapper.map(address, AddressDto.class);
    return ResponseEntity.ok(addressDto);
  }

  @GetMapping("/address/{employeeId}")
  public ResponseEntity<AddressDto> getAddressByEmployeeId(@PathVariable int employeeId) {
    System.out.println("8081");
    Address address = addressService.getAddressByEmployeeId(employeeId);
    AddressDto addressDto = modelMapper.map(address, AddressDto.class);
    return ResponseEntity.ok(addressDto);
  }
}
