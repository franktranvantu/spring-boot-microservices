package com.franktran.addressservice.dao;

import com.franktran.domain.entity.Address;
import com.franktran.domain.entity.AddressRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class AddressDao {

  private static final RowMapper<Address> ROW_MAPPER = (rs, index) -> {
    int id = rs.getInt("id");
    String line1 = rs.getString("line1");
    String line2 = rs.getString("line2");
    String city = rs.getString("city");
    String country = rs.getString("country");
    return new Address(id, line1, line2, city, country);
  };

  private static ResultSetExtractor<List<Address>> listExtractor() {
    List<Address> addresses = new ArrayList<>();
    return rs -> {
      while (rs.next()) {
        addresses.add(ROW_MAPPER.mapRow(rs, 1));
      }
      return addresses;
    };
  }

  private static ResultSetExtractor<Optional<Address>> optionalExtractor() {
    return rs -> rs.next() ? Optional.of(ROW_MAPPER.mapRow(rs, 1)) : Optional.empty();
  }

  private final JdbcTemplate jdbcTemplate;

  public AddressDao(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public List<Address> findAll() {
    String sql = "SELECT * FROM address";
    return jdbcTemplate.query(sql, listExtractor());
  }

  public Optional<Address> findById(int id) {
    String sql = "SELECT * FROM address WHERE id = ?";
    return jdbcTemplate.query(sql, optionalExtractor(), id);
  }


  public Optional<Address> findByEmployeeId(int employeeId) {
    String sql = "SELECT a.id as id, line1, line2, city, country FROM address a JOIN employee e ON a.id = e.address_id WHERE e.id = ?";
    return jdbcTemplate.query(sql, optionalExtractor(), employeeId);
  }
}
