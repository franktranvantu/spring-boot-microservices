## Build image
```dockerfile
docker build -t address-service:1.0 .
```

## Remove image
```dockerfile
docker rmi address-service
```

## Run container
```dockerfile
docker run --name address-service -p 8081:8081 -t address-service:1.0
```
## Stop container
```dockerfile
docker stop address-service
```
## Remove stopped container
```dockerfile
docker rm address-service:1.0
```
## Push image
```dockerfile
docker login --username franktranvantu
docker tag address-service:1.0 franktranvantu/address-service:1.0
docker push franktranvantu/address-service:1.0
```
